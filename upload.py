from flask import Blueprint, request, Flask_Uploads
upload_blueprint = Blueprint('upload blueprint', __name__)
csvfiles = UploadSet('csvfiles', FILES)
@upload_blueprint.route('/upload',methods = ['GET','POST'])
def upload():
    if request.method == 'POST' and 'csvfiles' in request.files:
        filename = csvfiles.save(request.files['csvfiles'])
        return filename


from flask import Flask
from upload import upload_blueprint
from convert import convert_blueprint

app = Flask(__name__)
app.register_blueprint(upload_blueprint)
app.register_blueprint(convert_blueprint)
app.config['UPLOADED_PATH']=os.path.join(app.root_path)

@app.route('/')
def hello_world():  # put application's code here
    return 'Hello World!'


if __name__ == '__main__':
    app.run()
